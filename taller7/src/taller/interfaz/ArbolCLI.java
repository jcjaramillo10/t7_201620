package taller.interfaz;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

import javax.imageio.stream.FileImageInputStream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import taller.mundo.Arbol;
import taller.mundo.NodoArbol;

public class ArbolCLI {
	
    private Scanner in;

    public ArbolCLI()
    {
        in = new Scanner(System.in);
    }
    
    public void mainMenu()
    {
       boolean finish = false;
       while(!finish)
       {	
           Screen.clear();
           System.out.println("------------------------------------------");
           System.out.println("-                                        -");
           System.out.println("-           Siembra de árboles           -");
           System.out.println("-                                        -");
           System.out.println("------------------------------------------");
           System.out.println("EL sistema para la plantación de árboles binarios\n");
          
           System.out.println("Menú principal:");
           System.out.println("-----------------");
           System.out.println("1. Cargar archivo con semillas");
           System.out.println("2. Salir");
           System.out.print("\nSeleccione una opción: ");
           int opt1 = in.nextInt();
           switch(opt1)
           {
                case 1:
                  recibirArchivo();
                  finish = true;
                  break;
                case 2:
                  finish = true;
                  break;
                default:
                  break;
           }
        }
    }

    public void recibirArchivo()
    {
         boolean finish = false;
         while(!finish)
         {
              Screen.clear();
              System.out.println("Recuerde que el archivo a cargar");
              System.out.println("debe ser un archivo properties");
              System.out.println("que tenga la propiedad in-orden,");
              System.out.println("la propiedad pre-orden (donde los ");
              System.out.println("elementos estén separados por comas) y");
              System.out.println("que esté guardado en la carpeta data.");
              System.out.println("");
              System.out.println("Introduzca el nombre del archivo '*.properties':");
              System.out.println("----------------------------------------------------");
              
              // TODO Leer el archivo .properties 
              
              String archivo = in.next();
              try {
            	  
				// TODO cargarArchivo
				// TODO Reconstruir árbol  
            	  Arbol<String> arbol = new Arbol<String>();
            	FileInputStream fis = new FileInputStream("./data/"+archivo+".properties");
            	Properties propiedades = new Properties();
            	propiedades.load(fis);
            	String preorden = propiedades.getProperty("preorden");
            	String inorden = propiedades.getProperty("inorden");
            	String[] elemento1 = preorden.split(",");
            	String[] elemento2 = inorden.split(",");
            	ArrayList<String> lista1 = new ArrayList<String>();
            	ArrayList<String> lista2 = new ArrayList<String>();
            	for (int i=0; i<elemento1.length; i++){
            		lista1.add(elemento1[i]);
            		lista2.add(elemento2[i]);
            	}
            	arbol.agregar(lista1, lista2);
          
            	Writer writer = new FileWriter("./data/arbolPlantado.json");
            	Gson gson = new Gson();
            	String json = gson.toJson(arbol.getRaiz());
            	writer.write(json);
            	writer.close();
				System.out.println("Ha plantado el árbol con éxito!\nPara verlo, dirijase a /data/arbolPlantado.json");
				System.out.println("Nota: ejecute Refresh (Clic derecho - Refresh) sobre la carpeta /data/ para actualizar y visualizar el archivo JSON");
				System.out.println("Presione 1 para salir");
				in.next();
				fis.close();
				finish=true;
				
			} catch (Exception e) {
	              System.out.println("Hubo un problema cargando el archivo:");
	              System.out.println(e.getMessage());
	              e.printStackTrace();

			}
              
         }
    }
} 