package taller.mundo;

import java.util.ArrayList;

public class Arbol<V> {
	
	private NodoArbol<V> raiz;
	private Queue<V> fila; 


	public Arbol ()
	{
		fila= new Queue<V>();
	}
	
	public boolean isEmpty(){
		return raiz == null;
	}

	
	public NodoArbol<V> getRaiz() {
		return raiz;
	}

	public void setRaiz(NodoArbol<V> raiz) {
		this.raiz = raiz;
	}

	public void agregar(ArrayList<V> preorden, ArrayList<V> inorden) {

		for (int i=0; i<preorden.size();i++){
			fila.enqueue(preorden.get(i));
		}
		
		V valor = fila.dequeue();
		raiz = armarArbol(valor,inorden);

	}

	private NodoArbol<V> armarArbol (V valor, ArrayList<V> lista){
		NodoArbol<V> base = raiz;
		if (base == null) base = new NodoArbol<V>(valor);

		if (lista.size() == 1)	 base = new NodoArbol<V>(lista.get(0));

		else{
			int pos = buscar(valor,lista);
			ArrayList<V> izq = darLista(0,pos,lista);
			ArrayList<V> der =	darLista(pos+1,lista.size(),lista);
			if (!izq.isEmpty()) base.setIzq(armarArbol(fila.dequeue(), izq));
			if (!der.isEmpty()) base.setDer(armarArbol(fila.dequeue(), der));
		}

		return base;
	}

	private ArrayList<V> darLista(int inicial, int fin, ArrayList<V> lista) {
		
		ArrayList<V> nueva = new ArrayList<V>();
		for(int k = inicial; k<fin; k++)
		{
			nueva.add(lista.get(k));
		}

		return nueva;
	}

	private int buscar(V valor, ArrayList<V> lista) {
		int pos=0;
		boolean encontro = false;
		for (int i=0; i<lista.size()&&!encontro;i++){
			if(valor.equals(lista.get(i))){
				pos = i;
				encontro=true;
			}
		}
		return pos;
	}

}	

