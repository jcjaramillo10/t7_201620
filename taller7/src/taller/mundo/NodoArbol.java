package taller.mundo;

public class NodoArbol<V> {
	
	private V val;
	private NodoArbol<V> izq;
	private NodoArbol<V> der;
	
	public NodoArbol(V val) {
		
		this.val = val;
		izq = null;
		der = null;
	}
	public NodoArbol<V> getIzq() {
		return izq;
	}
	public void setIzq(NodoArbol<V> izq) {
		this.izq = izq;
	}
	public NodoArbol<V> getDer() {
		return der;
	}
	public void setDer(NodoArbol<V> der) {
		this.der = der;
	}
	public V getVal() {
		return val;
	}
	public void setVal(V val) {
		this.val = val;
	}
	@Override
	public String toString() {
		String izquierda = (izq != null ? izq.toString():"");
		String derecha = (der != null ? der.toString():"");
		return "NodoArbol [raiz=" + val + ", izq=" + izquierda  + ", der=" + derecha + "]";
	}
	
	
}
