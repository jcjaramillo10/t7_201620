package taller.mundo;

/**
 * Clase que describe la fila con el protocolo FIFO
 * Este código es proporcionado por el profesor y el libro
 * package: estructuras-lineales.co.edu.uniandes.collections.linear
 */
public class Queue<T> {

	private Node<T> first;
	private Node<T> last;
	private int size; 
	
	public Queue(){
		this.first = null;
		this.last = null;
		this.size = 0 ;
	}
	
	
	
	public T dequeue(){
		T item = null;
		if(!isEmpty()){
			item  = first.getItem();
			first = first.getNext();
		}
		
		if(isEmpty()) last = null;
		size --;
		return item;
	}
	
	public void enqueue(T item){
		Node<T> oldLast = this.last;
		last = new Node<T>(item, null);
		if(isEmpty()){
			first = last;
		}else{
			oldLast.setNext(last);
		}
		size++;
	}
	
	public boolean isEmpty(){
		return first == null;
	}
	
	
	public int getSize() {
		return size;
	}
	
	public T getItemAtFirst() {
		if(isEmpty()){
			return null;
		}
		return first.getItem();
	}
	
	public static void main(String[] args){
		Queue<String> queue = new Queue<>();
		queue.enqueue("Hola");
		queue.enqueue("amigo");
		queue.enqueue("Chepe");
		
		String item = null;
		while( (item = queue.dequeue()) != null){
		
			System.out.println(item);
			System.out.println(queue.getSize());
		}
		
	}
	
}